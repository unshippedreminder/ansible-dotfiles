# How to use:

1. Host: `sudo systemctl start sshd`
2. Host: `toolbox enter`
3. Toolbox: `sudo dnf install python3-pip git`
4. Toolbox: `pip install ansible`
5. Toolbox: `git clone https://gitlab.com/unshippedreminder/ansible-dotfiles.git && cd ansible-dotfiles`
6. Toolbox: `ansible-playbook -i inventory.yml -e ansible_user=$USER -K local.yml`
