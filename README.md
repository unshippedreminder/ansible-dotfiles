# Ansible Dotfiles

## Scope

- Distinguish between multiple devices and only run relevant tasks.
- Install the applications i use.
- Configure them where possible.
  - (User) config files are synced via unshippedreminder/dotfiles
- Apply theming.

## Out of Scope

- Logging in to anything.
  - Such as file sync services, chat clients or browsers.

